@echo off
cd %cd%
cls
IF EXIST "%PROGRAMFILES(X86)%" (
  SET zip_os=x64
  SET jdk_os=x64
) ELSE (
  SET zip_os=ia32
  SET jdk_os=x86-32
)
echo Detected %jdk_os% bit system

SET curl_bin=curl.exe
SET curl_param=-Lk

SET zip_file=7za.exe
SET zip_os_file=7za_%zip_os%.exe
SET zip_url=https://github.com/develar/7zip-bin/raw/master/win/%zip_os%/%zip_file%

SET jdk21_version=21.0.6_7
SET jdk21_folder=jdk-21.0.6+7
rem + as %2B for url encoding % for escape
SET jdk21_release=jdk-21.0.6%%2B7
SET jdk21_file=OpenJDK21U-jdk_%jdk_os%_windows_hotspot_%jdk21_version%.zip
SET jdk21_wildcard_file=OpenJDK21U-jdk_%jdk_os%_windows_*.zip
SET jdk21_url=https://github.com/adoptium/temurin21-binaries/releases/download/%jdk21_release%/%jdk21_file%

SET android_sdk_version=11076708
SET android_sdk_file=commandlinetools-win-%android_sdk_version%_latest.zip
SET android_sdk_wildcard_file=commandlinetools-win-*.zip
SET android_sdk_url=https://dl.google.com/android/repository/%android_sdk_file%
SET android_sdk_folder=android-sdk
SET tools_folder=cmdline-tools

SET script_properties_file=script.properties
SET adhell3_branch=master
SET adhell3_author=fusionjack
IF EXIST "%script_properties_file%" (
   for /F "usebackq tokens=*" %%A in ("%script_properties_file%") do set %%A
  )
SET adhell3_file=adhell3-%adhell3_branch%.zip
SET adhell3_url=https://gitlab.com/%adhell3_author%/adhell3/-/archive/%adhell3_branch%/%adhell3_file%
SET adhell3_folder=adhell3-%adhell3_branch%
SET app_folder=app
SET libs_folder=app\libs

SET knoxsdk_jar_file=knoxsdk.jar
SET app_properties_file=app.properties
SET release_key_file=adhell3.jks

SET param1=%1
SET param2=%2
SET param3=%3

IF /i [%param1%] == [clean] IF /i [%param2%] == [setup] GOTO clean_setup_adhell3
IF /i [%param1%] == [clean] IF /i [%param2%] == [build] GOTO clean_build_adhell3
IF /i [%param1%] == [setup] GOTO setup_adhell3
IF /i [%param1%] == [build] GOTO build_adhell3
GOTO error
goto:eof

:clean_setup_adhell3
IF EXIST %jdk21_folder% (
  echo Deleting %jdk21_folder% ...
  del /f /s /q %jdk21_folder% 1>nul
  rmdir /s /q %jdk21_folder%
)

IF EXIST %android_sdk_folder% (
  echo Deleting %android_sdk_folder% ...
  del /f /s /q %android_sdk_folder% 1>nul
  rmdir /s /q %android_sdk_folder%
)

IF EXIST %tools_folder% (
  echo Deleting %tools_folder% ...
  del /f /s /q %tools_folder% 1>nul
  rmdir /s /q %tools_folder%
)

rem del %curl_file%
rem del %zip_os_file%

IF EXIST "%jdk21_wildcard_file%" (
  echo Deleting %jdk21_wildcard_file% ...
  del %jdk21_wildcard_file%
)

IF EXIST "%android_sdk_wildcard_file%" (
  echo Deleting %android_sdk_wildcard_file% ...
  del %android_sdk_wildcard_file%
)
goto setup_adhell3

:setup_adhell3
WHERE %curl_bin% >nul 2>nul
IF NOT ERRORLEVEL 0 (
  IF NOT EXIST "%curl_bin%" (
    echo Cannot find %curl_bin%
    echo Please download %curl_bin% for your system and copy it to script folder from: https://curl.se/download.html
    goto:eof
  )
) ELSE (
  echo Found %curl_bin%
)

IF NOT EXIST "%zip_os_file%" (
  IF /i [%zip_os%] == [x64] (
    echo Downloading %zip_file% ...
    %curl_bin% %curl_param% -o %zip_os_file% %zip_url% || goto :error
  ) ELSE (
    echo Please download, rename %zip_file% to %zip_os_file% and copy it to script folder from:
    echo %zip_url%
    goto:eof
  )
) ELSE (
  echo Found %zip_os_file%
)

IF NOT EXIST "%jdk21_wildcard_file%" (
  echo Downloading %jdk21_file% ...
  %curl_bin% %curl_param% -LO %jdk21_url% || goto :error
) ELSE (
  echo Found %jdk21_file%
)

IF NOT EXIST %jdk21_folder% (
  echo Extracting %jdk21_file% ...
  %zip_os_file% x %jdk21_file% || goto :error
)

IF NOT EXIST "%android_sdk_wildcard_file%" (
  echo Downloading %android_sdk_file% ...
  %curl_bin% %curl_param% -LO %android_sdk_url% || goto :error
) ELSE (
  echo Found %android_sdk_file%
)

IF NOT EXIST %tools_folder% (
  echo Extracting %android_sdk_file% ...
  %zip_os_file% x %android_sdk_file% || goto :error
)

IF NOT EXIST %android_sdk_folder% (
  echo Configuring Android SDK ...
  mkdir %android_sdk_folder% || goto :error
  SET JAVA_HOME=%cd%\%jdk21_folder%
  echo y|%tools_folder%\bin\sdkmanager "platform-tools" --sdk_root="%cd%\%android_sdk_folder%" || goto :error
)
goto:eof

:clean_build_adhell3
echo Cleaning build ...

IF EXIST "%adhell3_file%" (
  echo Deleting %adhell3_file% ...
  del %adhell3_file%
)

IF EXIST %adhell3_folder% (
  echo Deleting %adhell3_folder% folder ...
  del /f /s /q %adhell3_folder% 1>nul
  rmdir /s /q %adhell3_folder%
)

echo Getting latest adhell3 source code from gitlab ...
%curl_bin% %curl_param% -LO %adhell3_url% || goto :error

echo Extracting %adhell3_file% ...
%zip_os_file% x %adhell3_file% || goto :error
goto:build_adhell3

:build_adhell3
IF NOT EXIST %jdk21_folder% (
  echo Missing "%jdk21_folder%" folder, please run "adhell3 setup"
  exit /b
)

IF NOT EXIST %android_sdk_folder% (
  echo Missing "%android_sdk_folder%" folder, please run "adhell3 setup"
  exit /b
)

IF NOT EXIST "%knoxsdk_jar_file%" (
  echo Missing "%knoxsdk_jar_file%" file, please get it from Samsung KPP and put it in the same folder where this script is located
  exit /b
)

IF NOT EXIST "%app_properties_file%" (
  echo Missing "%app_properties_file%" file, please create it, set your application id and put it in the same folder where this script is located
  exit /b
)

IF NOT EXIST %adhell3_folder% (
  echo Getting latest adhell3 source code from gitlab ...
  %curl_bin% %curl_param% -LO %adhell3_url% || goto :error

  echo Extracting %adhell3_file% ...
  %zip_os_file% x %adhell3_file% || goto :error
)

SET JAVA_HOME=%cd%\%jdk21_folder%
SET ANDROID_HOME=%cd%\%android_sdk_folder%

IF /i [%param2%] == [install] goto install
IF /i [%param3%] == [install] goto install
IF /i [%param2%] == [release] goto release_build
IF /i [%param3%] == [release] goto release_build

echo Building apk ...
cd %adhell3_folder%
copy ..\%app_properties_file% %app_folder%
IF NOT EXIST %libs_folder% (
  mkdir %libs_folder% || goto :error
)
copy ..\%knoxsdk_jar_file% %libs_folder% || goto :error
call gradlew clean assembleDebug --no-daemon || goto :error
For /F "Tokens=*" %%I in ('findstr number app\build.properties') Do set build_number=%%I
set build_number=%build_number:~13,5%
copy app\build\outputs\apk\debug\app-debug.apk ..\adhell3_%build_number%.apk || goto :error
echo.
type app\build.properties
cd ..
goto:eof

:install
echo Building and installing apk ...
cd %adhell3_folder% || goto :error
copy ..\%app_properties_file% %app_folder% || goto :error
IF NOT EXIST %libs_folder% (
  mkdir %libs_folder% || goto :error
)
copy ..\%knoxsdk_jar_file% %libs_folder% || goto :error
call gradlew clean installDebug --no-daemon || goto :error
For /F "Tokens=*" %%I in ('findstr number app\build.properties') Do set build_number=%%I
set build_number=%build_number:~13,5%
copy app\build\outputs\apk\debug\app-debug.apk ..\adhell3_%build_number%.apk || goto :error
echo.
type app\build.properties
cd ..
goto:eof

:release_build
echo Building release apk ...
cd %adhell3_folder% || goto :error
copy ..\%app_properties_file% %app_folder% || goto :error
IF NOT EXIST %libs_folder% (
  mkdir %libs_folder% || goto :error
)
copy ..\%knoxsdk_jar_file% %libs_folder% || goto :error
copy ..\%release_key_file% app\%release_key_file%
call gradlew clean assembleRelease --no-daemon || goto :error
For /F "Tokens=*" %%I in ('findstr number app\build.properties') Do set build_number=%%I
set build_number=%build_number:~13,5%
copy app\build\outputs\apk\release\app-release-unsigned.apk ..\adhell3_%build_number%_unsigned.apk
copy app\build\outputs\apk\release\app-release.apk ..\adhell3_%build_number%_signed.apk
echo.
type app\build.properties
cd ..
goto:eof


:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
